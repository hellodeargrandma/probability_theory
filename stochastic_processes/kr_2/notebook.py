#!/usr/bin/env python
# coding: utf-8

# In[3]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# In[39]:


fname = "/home/grandma/projects/math/kr_2/schengen-visa-stats/2017-data-for-consulates_cut.csv"
fname = "/home/grandma/projects/math/kr_2/schengen-visa-stats/2018-data-for-consulates_cut.csv"
fname = "/home/grandma/projects/math/kr_2/app-store-apple-data-set-10k-apps/AppleStore.csv"
fname = "/home/grandma/projects/math/kr_2/youtube-trend-with-subscriber/USvideos_modified.csv"
fname = "/home/grandma/projects/math/kr_2/datasets/austin-animal-center-shelter-outcomes-and/aac_shelter_outcomes.csv"
fname = "/home/grandma/projects/math/kr_2/datasets/austin-animal-center-shelter-outcomes-and/aac_shelter_cat_outcome_eng.csv"
fname = "/home/grandma/projects/math/kr_2/datasets/aac_shelter_cat_outcome_eng/aac_shelter_cat_outcome_eng.csv"

col = 17

pd_data = pd.read_csv(fname, thousands=',')
pd_data[:2]


# In[40]:


# get only column 'col' and drop NaN rows
pd_col = pd_data.iloc[1:,col].dropna()
pd_col = pd_col[pd_col < 365]
# pd_data = pd_data.iloc[:,col].dropna().str.rstrip('%').astype('float') / 100.0
ar = np.array(pd_col)


# In[41]:


print("min: {}; max: {}".format(pd_col.min(), pd_col.max()))
print("Mean: {}".format(ar.mean()))
print("Variance: {}".format(ar.var()))
print("Variance fixed: {}".format(ar.var(ddof=1)))
print("Deviation: {}".format(ar.std()))
print("Deviation fixed: {}".format(ar.std(ddof=1)))


# In[43]:


ar_less = ar
counts, bins = np.histogram(ar_less, bins=30)
plt.plot(bins[:-1]+1, counts)
plt.show()


# In[44]:


pd_col_less = pd_col
pd_col_less.hist(bins=30)


# In[ ]:




