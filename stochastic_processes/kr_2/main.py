#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import pandas as pd
import numpy as np
import sys


def main():
    if len(sys.argv) != 3:
        print("Usage: {} <data_file.csv> <column>".format(sys.argv[0]))
        print("Programm will read the <column> column from given csv file.")
        exit(1)

    fname = sys.argv[1]
    col = int(sys.argv[2])

    data = np.array(pd.read_csv(fname))
    data = data[:,col]
    data = data.astype('uint')
    print(data.sort())


if __name__ == "__main__":
    main()
