#!/usr/bin/env python3

import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def get_mat(x_size=2, y_size=2, accuracy=2):
    # get random numbers
    prob_mat = np.random.rand(x_size, y_size)

    # make their sum be equal to 1
    prob_mat /= int(prob_mat.sum() + 1)

    # round it to 2 deciamal places
    prob_mat = np.round(prob_mat, accuracy)

    # add lost part to first element. sum is now equals to 1
    prob_mat[0, 0] = np.round(prob_mat[0, 0] + 1 - prob_mat.sum(), accuracy)

    return prob_mat


def get_n_z(size = 2):
    n = np.random.choice(np.arange(-9, 10), size, replace=False)
    z = np.random.choice(np.arange(-9, 10), size, replace=False).reshape(size, 1)

    return n, z


def dump_input(path, prob_mat, n, z):
    dict_object = dict(prob_mat=prob_mat.tolist(),
                       n=n.tolist(),
                       z=z.tolist())

    with open(path, 'w') as file_object:

        # Save dict data into the JSON file.
        json.dump(dict_object, file_object)


def load_input(path):
    dict_object = {}
    with open(path, 'r') as file_object:
        dict_object = json.load(file_object)

    return (np.array(dict_object['prob_mat']),
            np.array(dict_object['n']),
            np.array(dict_object['z']))


def get_stats(vals, probs):
    m = (vals * probs).sum()
    d = (probs * (vals - m) * (vals - m)).sum()
    s = np.sqrt(d)

    return m, d, s


def get_czn(n, z, prob_mat, mn, mz):
    return (prob_mat * n * z).sum() - mn * mz


def get_rzn(czn, sn, sz):
    return czn / (sn * sz)



def main():
    size = 5
    file_name = './input_data.json'

    # generate data and dump to file
    # prob_mat = get_mat(size, size, 2)
    # n, z = get_n_z(size)
    # dump_input(file_name, prob_mat, n, z)

    # or load data from file
    prob_mat, n, z = load_input(file_name)
    print("prob_mat:\n{}\nn:\n{}\nz:{}\n".format(
        prob_mat, n, z))


    # 1
    prob_mat_sum = prob_mat.sum()
    print("1.")
    print("sum = {}\n".format(prob_mat_sum))
    if prob_mat_sum != 1:
        print("ERROR: Probability matrix sum should be equal to 1.")
        return


    # 2
    pn = prob_mat.sum(axis=0)
    pz = prob_mat.sum(axis=1).reshape(size, 1)

    mn, dn, sn = get_stats(n, pn)
    mz, dz, sz = get_stats(z, pz)
    print("2.")
    print("pn:\n{}\n".format(pn))
    print("pz:\n{}\n".format(pz))
    print("mn = {}\ndn = {}\nsn = {}\n".format(mn, dn, sn))
    print("mz = {}\ndz = {}\nsz = {}\n".format(mz, dz, sz))


    #3
    czn = get_czn(n, z, prob_mat, mn, mz)
    rzn = get_rzn(czn, sn, sz)
    print("3.")
    print("czn:\n{}".format(czn))
    print("rzn:\n{}".format(rzn))
    print()


    #4
    def yx(x):
        return mn + rzn * (sn / sz) * (x - mz)

    def xy(y):
        return mz + rzn * sz * (y - mn) / sn

    x1 = [-2, 2]
    y1 = [0, 0]
    y1[0] = yx(x1[0])
    y1[1] = yx(x1[2])

    y2 = [-5, 2]
    x2 = [0, 0]
    x2[0] = xy(y2[0])
    x2[1] = xy(y2[1])

    plt.plot(x1, y1)
    plt.plot(x2, y2)

    print("4.")
    print("x1: {}".format(x1))
    print("y1: {}".format(y1))
    print("x2: {}".format(x2))
    print("y2: {}".format(y2))
    print()

    #5
    cond_prob_z = prob_mat / pz
    cond_prob_n = (prob_mat / pn).T

    myn = (cond_prob_z * n).sum(axis=1)
    mxz = (cond_prob_n * z.T).sum(axis=1)

    print("5.")
    print("cond_prob_mat_z:\n{}".format(cond_prob_z))
    print("cond_prob_mat_n:\n{}\n".format(cond_prob_n))
    print("myn:\n{}".format(myn))
    print("z:\n{}".format(z.T))
    print("mxz:\n{}".format(mxz))
    print("n:\n{}".format(n))
    print()

    plt.plot(z, myn, 'r*')
    plt.plot(mxz, n, 'ro')
    plt.grid(True)
    plt.show()



if __name__ == "__main__":
    main()
